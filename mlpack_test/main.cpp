
#include "mlpack_test.h"
#include "opencv_test.h"

int main()
{
	/*All functions are autonomous and can be tested by uncommenting corresponding line.*/

	//mlpack_rf_test();						//random forest test on german.csv data
	//mlpack_svm_test();					//SVM test on german.csv data
	//mlpack_k_means_test();				//k-means test on german.csv data
	//opencv_images_descriptor();			//converting images to data matrix with HOG global descriptor (pneumonia dataset)
	//opencv_classifier();					//opencv svm classifier on pneumonia dataset with HOG features
	//mlpack_linear_regression_test();     //linear regression test on haloween spendings dataset
	//opencv_spooky_images_descriptor();    //converting images to data matrix with HOG global descriptor (Michal's dataset)
	//opencv_spooky_classifier();			//opencv svm classifier on Michal's dataset with HOG features
	//mlpack_rf_spooky_test();				//random forest test on Michal's dataset with HOG features
	//mlpack_k_means_spooky_test();			 //k-means test on Michal's dataset with HOG features
	//mlpack_svm_candy_test();                //svm test on candy-data dataset with constant explained variable discretization
	//opencv_spooky_local_features_descriptor_extractor_test();      // local features extraction test on pumpkins images from michal's dataset
	//opencv_spooky_local_features_descriptor_extractor_full();            //full iamge classification algorithm on michal's dataset (local features extraction and description with SIFT, bag of visual words, SVM classiffier)
	//mlpack_linear_regression_1_variable_test();                   //linear regression test on haloween spendings dataset (1 explanatory variable)
	bowDE_svm_prediction();						//loading pretrained BOW + SVM and single image prediction
}