﻿

#include <iostream>
#include <mlpack/core.hpp>
#include <mlpack/mlpack_export.hpp>
#include <mlpack/core/data/load.hpp>

#include <mlpack/core/cv/cv_base.hpp>
#include <mlpack/prereqs.hpp>
#include <mlpack/core/cv/k_fold_cv.hpp>
#include <mlpack/core/cv/metrics/accuracy.hpp>
#include <mlpack/core/cv/metrics/precision.hpp>
#include <mlpack/core/cv/metrics/f1.hpp>
#include <mlpack/core/cv/metrics/average_strategy.hpp>

#include <mlpack/methods/random_forest/random_forest.hpp>
#include <mlpack/methods/decision_tree/decision_tree.hpp>
#include <mlpack/methods/decision_tree/multiple_random_dimension_select.hpp>
#include <mlpack/methods/decision_tree/random_dimension_select.hpp>

#include<mlpack/methods/linear_svm/linear_svm.hpp>
#include<mlpack/methods/linear_svm/linear_svm_impl.hpp>

#include <mlpack/methods/kmeans/kmeans.hpp>

#include <mlpack/methods/linear_regression/linear_regression.hpp>

#include <mlpack/core/data/scaler_methods/mean_normalization.hpp>

#include <mlpack/core/data/split_data.hpp>

using namespace mlpack::kmeans;
using namespace std;
using namespace arma;
using namespace mlpack;
using namespace mlpack::data;
using namespace mlpack::cv;
using namespace mlpack::svm;
using namespace mlpack::regression;




void mlpack_rf_test()
{
    cout << "Model Random Forest!\n";
    /*Loading the dataset and getting labels (last column of the file). 
    The data matrix is transposed in comparison to standard approach (stores features in rows), that's required for mlpack models. */
    mat dataset;
    bool loaded = Load("german.csv", dataset);

    Row<size_t> labels;
    labels = conv_to<Row<size_t>>::from(dataset.row(dataset.n_rows - 1));
    dataset.shed_row(dataset.n_rows - 1);

    /*model hyperparameters*/
    cout << "\ntraining...";
    const size_t numClasses = 2;
    const size_t minimumLeafSize = 5;
    const size_t numTrees = 10;

    
    mlpack::tree::RandomForest<> rf;
    rf = mlpack::tree::RandomForest<>(dataset, labels,
        numClasses, numTrees, minimumLeafSize);

    Row<size_t> predictions;
    rf.Classify(dataset, predictions);

    const size_t correct = accu(predictions == labels);
    cout << "\nTraining Accuracy: " << (double(correct) / double(labels.n_elem));

    // Cross-Validation
    cout << "\nCross-Validating...";
    const size_t k = 10;

    KFoldCV<mlpack::tree::RandomForest<>, Accuracy> cv(k,
        dataset, labels, numClasses);

    double cvAcc = cv.Evaluate(numTrees, minimumLeafSize);
    cout << "\nKFoldCV Accuracy: " << cvAcc;

    double cvPrecision = Precision<Binary>::Evaluate(rf, dataset, labels);
    cout << "\nPrecision: " << cvPrecision;

    double cvF1 = F1<Binary>::Evaluate(rf, dataset, labels);
    cout << "\nF1: " << cvF1;

    cout << "\nSaving model...";
    Save("mymodel.xml", "model", rf, false);

    cout << "\nLoading model...";
    Load("mymodel.xml", "model", rf);

    // (6) Classify a new sample
    cout << "\nClassifying a new sample...";
    // Should classify as "1"
    mat sample("2 12 2 13 1 2 2 1 3 24 3 1 1 1 1 1 0 1 0 1 0 0 0");

    mat probabilities;

    rf.Classify(sample, predictions, probabilities);

    u64 result = predictions.at(0);

    cout << "\nClassification result: " << result << " , Probabilities: " <<
        probabilities.at(0) << "/" << probabilities.at(1);

    cout << "\n[SAMPLE:END]\n";
}


void mlpack_svm_test()
{
    std::cout << "Model SVM!\n";
    /*Loading the dataset and getting labels (last column of the file).
    The data matrix is transposed in comparison to standard approach (stores features in rows), whhich is required for mlpack models. */
    mat dataset;
    bool loaded = Load("german.csv", dataset);
    

    Row<size_t> labels;
    labels = conv_to<Row<size_t>>::from(dataset.row(dataset.n_rows - 1));
    dataset.shed_row(dataset.n_rows - 1);

    cout << "\ntraining...";
    const size_t numClasses = 2;

    /*Linear SVM with default parameters*/
    LinearSVM<> lsvm;
    lsvm = LinearSVM<>(dataset, labels, numClasses);
    Row<size_t> predictions;
    lsvm.Classify(dataset, predictions);

    const size_t correct = accu(predictions == labels);
    cout << "\nTraining Accuracy: " << (double(correct) / double(labels.n_elem));

    // (3) Cross-Validation
    cout << "\nCross-Validating...";
    const size_t k = 10;

    KFoldCV<LinearSVM<>, Accuracy> cv(k,
        dataset, labels, numClasses);

    double cvPrecision = Precision<Binary>::Evaluate(lsvm, dataset, labels);
    cout << "\nPrecision: " << cvPrecision;

    double cvF1 = F1<Binary>::Evaluate(lsvm, dataset, labels);
    cout << "\nF1: " << cvF1;

    cout << "\nSaving model...";
    Save("mymodel.xml", "model", lsvm, false);

    cout << "\nLoading model...";
    Load("mymodel.xml", "model", lsvm);

    // (6) Classify a new sample
    cout << "\nClassifying a new sample...";
    // Should classify as "1"
    mat sample("2 12 2 13 1 2 2 1 3 24 3 1 1 1 1 1 0 1 0 1 0 0 0");
    sample = trans(sample);

    mat probabilities;
    lsvm.Classify(sample, predictions, probabilities);

    cout << "\n[SAMPLE:END]\n";
}

void mlpack_k_means_test()
{
    mat dataset;
    /*Loading the dataset and getting labels (last column of the file).
    The data matrix is transposed in comparison to standard approach (stores features in rows), whhich is required for mlpack models. */
    cout << "Model K-means!\n";
    bool loaded = Load("german.csv", dataset);

    Row<size_t> labels;
    labels = conv_to<Row<size_t>>::from(dataset.row(dataset.n_rows - 1));
    dataset.shed_row(dataset.n_rows - 1);

    cout << "training \n";
    // The number of clusters we are obtaining.
    size_t clusters;
    clusters = 2;
    // A vector which is going to hold samples assignments.
    Row<size_t> assignments;
    // This will hold the centroids of the finished clusters.
    mat centroids;
    //Number of model's iterations
    size_t n_it = 2000;
    
    //different distance metrics can be used, like mlpack::metric::ChebyshevDistance, mlpack::metric::EuclideanDistance or mlpack::metric::ManhattanDistance
    KMeans<mlpack::metric::EuclideanDistance> k(n_it);
  
    //clustering
    k.Cluster(dataset, clusters,assignments,centroids);

    const size_t correct = accu(assignments == labels);
    cout << "\n classification Accuracy: " << (double(correct) / double(labels.n_elem));

    cout << "\n[SAMPLE:END]\n";
}

void mlpack_linear_regression_test()
{
    mat dataset, test_set;
    cout << "Model linear regression!\n";
    /*Loading the dataset and getting labels (last column of the file).
    The data matrix is transposed in comparison to standard approach (stores features in rows), whhich is required for mlpack models. */
    bool loaded = Load("spending_totals.csv", dataset);
    Row<double> responses, test_responses;

    //remove header with variable names
    dataset.shed_col(0);

    //remove column with sum of variables
    dataset.shed_row(1);

    //get explanatory variable
    responses = conv_to<Row<double>>::from(dataset.row(1));
    dataset.shed_row(1);

    //train-test division
    test_set = dataset.cols(dataset.n_cols - 2, dataset.n_cols - 1);
    test_responses = responses.cols(responses.n_cols - 2, responses.n_cols - 1);
    dataset.shed_cols(dataset.n_cols - 2, dataset.n_cols - 1);
    responses.shed_cols(dataset.n_cols - 2, dataset.n_cols - 1);

    //Features selection
    mat features, features_test;
    features_test = test_set.rows(1, test_set.n_rows - 1);
    features = dataset.rows(1, dataset.n_rows - 1);

    //Data normalization, fitting scaler only to train data is considered a good practice
    MeanNormalization scale;
    mat normalized, normalized_test;
    scale.Fit(features);
    scale.Transform(features, normalized);
    scale.Transform(features_test, normalized_test);
    
    //lambda hyperparameter of the regressor
    double lmb = 0.5;

    //Linear regressor training and testing
    Row<double> predictions;
    LinearRegression lr(normalized, responses, lmb);
    lr.Predict(normalized_test, predictions);

    //printing test results
    for(int i=0; i< test_responses.n_cols; i++)
    cout << "year: " << test_set(0,i) << " predicted costumes spendings: " << predictions(i) << "m, real value: " << test_responses(i) << "m\n";

    cout << "\n[SAMPLE:END]\n";
}

void mlpack_linear_regression_1_variable_test()
{
    mat dataset, test_set;
    cout << "Model linear regression!\n";
    /*Loading the dataset and getting labels (last column of the file).
    The data matrix is transposed in comparison to standard approach (stores features in rows), whhich is required for mlpack models. */
    bool loaded = Load("spending_totals.csv", dataset);
    Row<double> responses, test_responses;

    //remove header with variable names
    dataset.shed_col(0);

    //get explained variable
    responses = conv_to<Row<double>>::from(dataset.row(2));

    //train-test division
    test_set = dataset.cols(dataset.n_cols - 2, dataset.n_cols - 1);
    test_responses = responses.cols(responses.n_cols - 2, responses.n_cols - 1);
    dataset.shed_cols(dataset.n_cols - 2, dataset.n_cols - 1);
    responses.shed_cols(responses.n_cols - 2, responses.n_cols - 1);

    //Features selection
    mat features, features_test;
    features_test = test_set.rows(3,3);
    features = dataset.rows(3,3);

    //lambda hyperparameter of the regressor
    double lmb = 0.5;

    //Linear regressor training and testing
    Row<double> predictions_train, predictions_test;
    LinearRegression lr(features, responses, lmb);
    lr.Predict(features, predictions_train);
    lr.Predict(features_test, predictions_test);

    //printing test results
    cout << "\nrange:";
    cout << "\n" << features << features_test;
    cout << "\n predictions train:";
    cout << "\n" << predictions_train;
    cout << "\n predictions test:";
    cout << "\n" << predictions_test;
    cout << "\n responses:";
    cout << "\n" << responses;

    cout << "\n[SAMPLE:END]\n";
}

void mlpack_rf_spooky_test()
{
    cout << "Model Random Forest!\n";
    /*Loading the dataset and getting labels (last column of the file).
    The data matrix is transposed in comparison to standard approach (stores features in rows), that's required for mlpack models. */
    mat dataset;
    bool loaded = Load("spooky_features.csv", dataset);

    Row<size_t> labels;
    labels = conv_to<Row<size_t>>::from(dataset.row(0));
    dataset.shed_row(0);

    /*model hyperparameters*/
    cout << "\ntraining...";
    const size_t numClasses = 4;
    const size_t minimumLeafSize = 5;
    const size_t numTrees = 10;


    mlpack::tree::RandomForest<> rf;
    rf = mlpack::tree::RandomForest<>(dataset, labels,
        numClasses, numTrees, minimumLeafSize);

    Row<size_t> predictions;
    rf.Classify(dataset, predictions);

    const size_t correct = accu(predictions == labels);
    cout << "\nTraining Accuracy: " << (double(correct) / double(labels.n_elem));

    // Cross-Validation
    cout << "\nCross-Validating...";
    const size_t k = 10;

    KFoldCV<mlpack::tree::RandomForest<>, Accuracy> cv(k,
        dataset, labels, numClasses);

    double cvAcc = cv.Evaluate(numTrees, minimumLeafSize);
    cout << "\nKFoldCV Accuracy: " << cvAcc;


    cout << "\nSaving model...";
    Save("mymodel.xml", "model", rf, false);

    cout << "\nLoading model...";
    Load("mymodel.xml", "model", rf);

    cout << "\n[SAMPLE:END]\n";
}


void mlpack_svm_spooky_test()
{
    std::cout << "Model SVM!\n";

    /*Loading the dataset and getting labels (last column of the file).
    The data matrix is transposed in comparison to standard approach (stores features in rows), whhich is required for mlpack models. */
    mat dataset;
    bool loaded = Load("spooky_features.csv", dataset);

    Row<size_t> labels;
    labels = conv_to<Row<size_t>>::from(dataset.row(0));
    dataset.shed_row(0);

    cout << "\ntraining...\n";
    const size_t numClasses = 4;

    /*Linear SVM with default parameters*/
    LinearSVM<> lsvm;
    lsvm = LinearSVM<>(dataset, labels, numClasses);
    Row<size_t> predictions;
    lsvm.Classify(dataset, predictions);

    const size_t correct = accu(predictions == labels);
    cout << "\nTraining Accuracy: " << (double(correct) / double(labels.n_elem));

    // (3) Cross-Validation
    cout << "\nCross-Validating...";
    const size_t k = 10;
    const size_t minimumLeafSize = 5;
    const size_t numTrees = 10;

    KFoldCV<LinearSVM<>, Accuracy> cv(k,
        dataset, labels, numClasses);
    double cvAcc = cv.Evaluate(numTrees, minimumLeafSize);
    cout << "\nKFoldCV Accuracy: " << cvAcc;

    cout << "\nSaving model...";
    Save("mymodel.xml", "model", lsvm, false);

    cout << "\nLoading model...";
    Load("mymodel.xml", "model", lsvm);

    cout << "\n[SAMPLE:END]\n";
}

void mlpack_k_means_spooky_test()
{
    cout << "Model K-means!\n";

    /*Loading the dataset and getting labels (last column of the file).
    The data matrix is transposed in comparison to standard approach (stores features in rows), whhich is required for mlpack models. */
    mat dataset;
    bool loaded = Load("spooky_features.csv", dataset);

    Row<size_t> labels;
    labels = conv_to<Row<size_t>>::from(dataset.row(0));
    dataset.shed_row(0);

    cout << "training \n";
    // The number of clusters we are obtaining.
    size_t clusters;
    clusters = 4;
    // A vector which is going to hold samples assignments.
    Row<size_t> assignments;
    // This will hold the centroids of the finished clusters.
    mat centroids;
    //Number of model's iterations
    size_t n_it = 2000;

    //different distance metrics can be used, like mlpack::metric::ChebyshevDistance, mlpack::metric::EuclideanDistance or mlpack::metric::ManhattanDistance
    KMeans<mlpack::metric::EuclideanDistance> k(n_it);

    //clustering
    k.Cluster(dataset, clusters, assignments, centroids);

    const size_t correct = accu(assignments == labels);
    cout << "\n classification Accuracy: " << (double(correct) / double(labels.n_elem));

    cout << "\n[SAMPLE:END]\n";
}

Row<size_t> disc_variable(Row<double> responses, Row<size_t> labels)
{
    //Procedure of constant label discretization.
    int thresholds_num = 2;
    double thresh_interval = (responses.max() - responses.min()) / double(thresholds_num);
    for (int i = 0; i < responses.n_cols - 1; i++)
    {
        labels[i] = (responses[i] - responses.min()) / thresh_interval - 0.00001;
    }
    return labels;
}

void mlpack_svm_candy_test()
{
    std::cout << "Model SVM!\n";

    /*Loading the dataset and getting labels (last column of the file).
    The data matrix is transposed in comparison to standard approach (stores features in rows), which is required for mlpack models. */
    mat dataset;
    bool loaded = Load("candy-data.csv", dataset);

    //remove header with variable names
    dataset.shed_col(0);

    //getting successfulness explained variable
    Row<double> responses;
    responses = conv_to<Row<double>>::from(dataset.row(dataset.n_rows-1));
    dataset.shed_row(dataset.n_rows - 1);

    //removing first column (candy names)
    dataset.shed_row(0);

    //Changing constant successfulness variable to categorical labels
    Row<size_t> labels(responses.n_cols);
    labels = disc_variable(responses, labels);
    mat trainX, testX;
    Row<size_t> trainY, testY;

    mlpack::data::Split(dataset, labels, trainX, testX, trainY, testY, 0.2);

    /*Linear SVM with default parameters*/
    cout << "\ntraining...";
    const size_t numClasses = 2;
    LinearSVM<> lsvm;
    lsvm = LinearSVM<>(trainX, trainY, numClasses);
    Row<size_t> predictions;
    lsvm.Classify(testX, predictions);

    //getting model weights.
    mat params = lsvm.Parameters();
    Row<double> unsuccessfulvec = conv_to<Row<double>>::from(params.col(0));
    cout << "Feature weights for unsuccessfull sweets:\n";
    cout << unsuccessfulvec << "\n";
    cout << "Feature weights for successfull sweets:\n";
    Row<double> successfulvec = conv_to<Row<double>>::from(params.col(1));
    cout << successfulvec << "\n";

    size_t correct = accu(predictions == testY);
    cout << "\nTraining Accuracy: " << (double(correct) / double(testY.n_elem));

    // (3) Cross-Validation
    cout << "\nCross-Validating...";
    const size_t k = 10;

    KFoldCV<LinearSVM<>, Accuracy> cv(k,
        dataset, labels, numClasses);
    double cvAcc = cv.Evaluate();
    cout << "\nKFoldCV Accuracy: " << cvAcc << "\n";

    //Same steps with random forest model
    std::cout << "\nModel Random Forest!\n";
    const size_t minimumLeafSize = 5;
    const size_t numTrees = 15;

    mlpack::tree::RandomForest<> rf;
    rf = mlpack::tree::RandomForest<>(trainX, trainY,
        numClasses, numTrees, minimumLeafSize);
    rf.Classify(testX, predictions);
    correct = accu(predictions == testY);
    cout << "\nTest Accuracy: " << (double(correct) / double(testY.n_elem));

    // (3) Cross-Validation
    cout << "\nCross-Validating...";

    KFoldCV<mlpack::tree::RandomForest<>, Accuracy> rfcv(k,
        dataset, labels, numClasses);
    double rfcvAcc = rfcv.Evaluate(numTrees, minimumLeafSize);
    cout << "\nKFoldCV Accuracy: " << rfcvAcc << "\n";
    
    cout << "\n[SAMPLE:END]\n";
}

// Uruchomienie programu: Ctrl + F5 lub menu Debugowanie > Uruchom bez debugowania
// Debugowanie programu: F5 lub menu Debugowanie > Rozpocznij debugowanie

// Porady dotyczące rozpoczynania pracy:
//   1. Użyj okna Eksploratora rozwiązań, aby dodać pliki i zarządzać nimi
//   2. Użyj okna programu Team Explorer, aby nawiązać połączenie z kontrolą źródła
//   3. Użyj okna Dane wyjściowe, aby sprawdzić dane wyjściowe kompilacji i inne komunikaty
//   4. Użyj okna Lista błędów, aby zobaczyć błędy
//   5. Wybierz pozycję Projekt > Dodaj nowy element, aby utworzyć nowe pliki kodu, lub wybierz pozycję Projekt > Dodaj istniejący element, aby dodać istniejące pliku kodu do projektu
//   6. Aby w przyszłości ponownie otworzyć ten projekt, przejdź do pozycji Plik > Otwórz > Projekt i wybierz plik sln
