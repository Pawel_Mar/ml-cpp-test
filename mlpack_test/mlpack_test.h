#pragma once

void mlpack_svm_test();
void mlpack_rf_test();
void mlpack_k_means_test();
void mlpack_linear_regression_test();
void mlpack_svm_spooky_test();
void mlpack_rf_spooky_test();
void mlpack_k_means_spooky_test();
void mlpack_svm_candy_test();
void mlpack_linear_regression_1_variable_test();