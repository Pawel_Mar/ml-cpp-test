#include <opencv2/opencv.hpp>
#include <opencv2/ml/ml.hpp>
#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <boost/filesystem.hpp>
#include <list>


using namespace cv;
using namespace cv::ml;
using namespace std;
using namespace boost::filesystem;


Mat hog_descriptor(string dir)
{
	//Defining descriptor parameters, see more on https://learnopencv.com/histogram-of-oriented-gradients/ 
	//and https://docs.opencv.org/4.x/d5/d33/structcv_1_1HOGDescriptor.html
	Mat feature_matrix;
	HOGDescriptor hog(
		Size(64, 64), //winSize
		Size(32, 32), //blocksize
		Size(32, 32), //blockStride,
		Size(32, 32), //cellSize,
		9, //nbins,
		1);//Use signed gradients
	int counter = 0;
	int class_num = 0;
	vector<float> labels = {};

	//loop for traversing dataset folder, loading, describing and labeling images
	for (directory_entry& dec_class : directory_iterator(dir))
	{
		cout << dec_class.path() << " directory was opened.\n";
		//#pragma omp parallel for
		for (directory_entry& img_path : directory_iterator(dec_class.path().string()))
		{
			//reading the image
			Mat img = imread(img_path.path().string());

			//convertion to grayscale
			Mat gray;
			cvtColor(img, gray, COLOR_BGR2GRAY);

			//image resizing
			resize(gray, gray, Size(500, 500), INTER_LINEAR);

			//computing descriptor
			vector<float> description;
			hog.compute(gray, description);
			feature_matrix.push_back(Mat(description).t());

			//labeling the image
			labels.push_back(class_num);

			if (counter % 100 == 0)
			{
				cout << counter << " images from the directory processed.\n";
			}

			counter++;
		}
		class_num++;
	}

	//labels and feature matrix concatenation in order to save it to .csv
	hconcat(Mat(labels), feature_matrix, feature_matrix);
	return feature_matrix;
}


void opencv_images_descriptor()
{
	/*Procedure to describe all images in dataset folder as feature vectors of standard size. 
	OpenCV HOG global descriptor is used, but procedure can be adjusted to different types of descriptors.*/

	ofstream outFile;

	//Paths to train and test sets.
	string dir_train = "chest_xray/train"; 
	string dir_test = "chest_xray/test";

	//reading and describing the images procedure
	Mat feature_matrix_train = hog_descriptor(dir_train);
	Mat feature_matrix_test = hog_descriptor(dir_test);

	//saving matrices to .csv files
	outFile.open("features_train.csv", std::ofstream::out | std::ofstream::trunc);
	outFile << cv::format(feature_matrix_train, Formatter::FMT_CSV) << std::endl;
	outFile.close();

	outFile.open("features_test.csv", std::ofstream::out | std::ofstream::trunc);
	outFile << cv::format(feature_matrix_test, Formatter::FMT_CSV) << std::endl;
	outFile.close();
	cout << "Done!\n";
}

void opencv_classifier()
{
	//Reading .csv data with TrainData class, train and test sets are stored in separate files.
	cout << "reading data...\n";
	Ptr<TrainData> trainData = TrainData::loadFromCSV("features_train.csv", 0, 0, -1);
	Ptr<TrainData> testData = TrainData::loadFromCSV("features_test.csv", 0, 0, -1);

	Mat trainX = trainData->getTrainSamples();
	Mat trainY = trainData->getTrainResponses();
	
	Mat testX = testData->getTrainSamples();
	Mat testY = testData->getTrainResponses();

	//It is possible to conduct train-test split yourself from single file with:
	/*trainData->setTrainTestSplitRatio(0.8);
	Mat trainX = trainData->getTrainSamples();
	Mat trainY = trainData->getTrainResponses();
	Mat testX = trainData->getTestSamples();
	Mat testY = trainData->getTestResponses();*/

	//Converting labels to integers
	trainY.convertTo(trainY, CV_32S);
	testY.convertTo(testY, CV_32S);

	//Initializing and training SVM classifier
	cout << "Initializing and training classifier...\n";
	Ptr<SVM> svm = SVM::create();
	svm->setType(SVM::C_SVC);
	svm->setKernel(SVM::LINEAR);
	svm->setTermCriteria(TermCriteria(TermCriteria::MAX_ITER, 100, 1e-6));
	svm->train(trainX, ROW_SAMPLE, trainY);

	//testing classifier
	cout << "testing classifier...\n";
	Mat predRes, diff;
	svm->predict(testX, predRes);
	predRes.convertTo(predRes, CV_32S);
	float accuracy = float(countNonZero(predRes == testY)) / float(testY.rows);

	//printing a bunch of test results
	for(int i=0; i<500;i++)
	{ 
		cout << "true label: "<< testY.at<float>(i, 0) << " prediction: " << predRes.at<float>(i, 0) << "\n";
	}
	cout << "\nclassification accuracy: " << accuracy;

}


void opencv_spooky_images_descriptor()
{
	/*Procedure to describe all images in dataset folder as feature vectors of standard size.
	OpenCV HOG global descriptor is used, but procedure can be adjusted to different types of descriptors.*/

	ofstream outFile;

	//Paths to train and test sets.
	string dir_train = "webinar_pics/webinar_pics";

	//reading and describing the images procedure
	Mat feature_matrix_train = hog_descriptor(dir_train);

	//saving matrices to .csv files
	outFile.open("spooky_features.csv", std::ofstream::out | std::ofstream::trunc);
	outFile << cv::format(feature_matrix_train, Formatter::FMT_CSV) << std::endl;
	outFile.close();
	cout << "Done!\n";
}

void opencv_spooky_classifier()
{
	//Reading .csv data with TrainData class, train and test sets are stored in separate files. spooky_features .csv contains features extracted by HOG global descriptor.
	cout << "reading data...\n";
	Ptr<TrainData> trainData = TrainData::loadFromCSV("spooky_features.csv", 0, 0, -1);

	
	//It is possible to conduct train-test split yourself from single file with:
	trainData->setTrainTestSplitRatio(0.8);
	Mat trainX = trainData->getTrainSamples();
	Mat trainY = trainData->getTrainResponses();
	Mat testX = trainData->getTestSamples();
	Mat testY = trainData->getTestResponses();

	//Converting labels to integers
	trainY.convertTo(trainY, CV_32S);
	testY.convertTo(testY, CV_32S);

	//Initializing and training SVM classifier
	cout << "Initializing and training classifier...\n";
	Ptr<SVM> svm = SVM::create();
	svm->setType(SVM::C_SVC);
	svm->setKernel(SVM::INTER);
	svm->setTermCriteria(TermCriteria(TermCriteria::MAX_ITER, 300, 1e-6));
	svm->train(trainX, ROW_SAMPLE, trainY);

	//testing classifier
	cout << "testing classifier...\n";
	Mat predRes, diff;
	svm->predict(testX, predRes);
	predRes.convertTo(predRes, CV_32S);
	float accuracy = float(countNonZero(predRes == testY)) / float(testY.rows);

	//printing a bunch of test results
	for (int i = 0; i < 20; i++)
	{
		cout << "true label: " << testY.at<float>(i, 0) << " prediction: " << predRes.at<float>(i, 0) << "\n";
	}
	cout << "\nclassification accuracy: " << accuracy;

}

void opencv_spooky_local_features_descriptor_extractor_test()
{
	Mat image1 = imread("./webinar_pics/webinar_pics/ghosts/31.png");
	Mat image2 = imread("./webinar_pics/webinar_pics/ghosts/79.png");

	//convertion to grayscale
	Mat gray1, gray2;
	resize(image1, image1, Size(500, 500), INTER_LINEAR);
	resize(image2, image2, Size(500, 500), INTER_LINEAR);
	cvtColor(image1, gray1, COLOR_BGR2GRAY);
	cvtColor(image2, gray2, COLOR_BGR2GRAY);

	// define vocabulary size
	int vocab_size = 32;
	int n_features = 128;

	// Create a trainer with vocab size 
	BOWKMeansTrainer bow_trainer(vocab_size);

	//Initializing SIFT feature detector. For more detectors see https://www.codetd.com/en/article/12588160
	Ptr<SIFT> feature_extractor = SIFT::create(n_features);
	Mat descriptor;


	//Detecting features and their descriptors
	vector<KeyPoint> keypoints1, keypoints2;
	feature_extractor->detectAndCompute(gray1, Mat(), keypoints1, descriptor);
	bow_trainer.add(descriptor);
	feature_extractor->detectAndCompute(gray2, Mat(), keypoints2, descriptor);
	bow_trainer.add(descriptor);
	Mat vocab = bow_trainer.cluster();

	// choosing feature descriptor and matcher (https://docs.opencv.org/3.4/d5/d6f/tutorial_feature_flann_matcher.html) for bowDE
	BOWImgDescriptorExtractor bowDE(SIFT::create(n_features), DescriptorMatcher::create("FlannBased"));

	// setting bowDE vocabulary
	bowDE.setVocabulary(vocab);

	Mat train;

	// computing feature histograms with bowDE
	bowDE.compute(gray1, keypoints1, descriptor);
	train.push_back(descriptor);
	cout << descriptor;
	bowDE.compute(gray2, keypoints2, descriptor);
	train.push_back(descriptor);
	cout << descriptor;

	//displaying pictures with marked features
	Mat output1, output2;
	drawKeypoints(image1, keypoints1, output1, Scalar(0, 0, 255));
	drawKeypoints(image2, keypoints2, output2, Scalar(0, 0, 255));
	imwrite("./presentation_images/correct.png", image1);
	imwrite("./presentation_images/incorrect.png", image2);
	imwrite("./presentation_images/correct_features.png", output1);
	imwrite("./presentation_images/incorrect_features.png", output2);
	imshow("Features detected pic1", output1);
	imshow("Features detected pic2", output2);
	
	// Displaying the images
	waitKey(0);
}

BOWImgDescriptorExtractor bowDE_preparation(string dir, int n_features = 128, int vocab_size = 32)
{
	// Create a trainer with vocab size 
	BOWKMeansTrainer bow_trainer(vocab_size);

	//Initializing SIFT feature detector. For more detectors see https://www.codetd.com/en/article/12588160
	Ptr<SIFT> feature_extractor = SIFT::create(n_features);
	
	int counter = 0;
	int class_num = 0;

	for (directory_entry& dec_class : directory_iterator(dir))
	{
		cout << dec_class.path() << " directory was opened.\n";
		for (directory_entry& img_path : directory_iterator(dec_class.path().string()))
		{
			//reading the image
			Mat img = imread(img_path.path().string());

			//convertion to grayscale
			Mat gray;
			cvtColor(img, gray, COLOR_BGR2GRAY);

			//image resizing
			resize(gray, gray, Size(500, 500), INTER_LINEAR);

			// SIFT feature detection and description
			Mat descriptor;
			vector<KeyPoint> keypoints;
			feature_extractor->detectAndCompute(gray, Mat(), keypoints, descriptor);
			bow_trainer.add(descriptor);

			if (counter % 100 == 0)
			{
				cout << counter << " images from the directory processed.\n";
			}
			counter++;
		}
		class_num++;
	}

	// local features clustering to create vocabulary
	Mat vocab = bow_trainer.cluster();	// choosing feature descriptor and matcher (https://docs.opencv.org/3.4/d5/d6f/tutorial_feature_flann_matcher.html) for bowDE
	FileStorage storage_w("trained_models/BOW_vocab.xml", FileStorage::WRITE);
	storage_w << "vocabulary" << vocab;
	storage_w.release();
	/*Mat vocab_read;
	FileStorage storage_r("trained_models/BOW_vocab.xml", FileStorage::READ);
	storage_r["vocabulary"] >> vocab_read;
	storage_r.release();*/
	BOWImgDescriptorExtractor bowDE(SIFT::create(n_features), DescriptorMatcher::create("FlannBased"));

	//setting bowDE vocabulary
	bowDE.setVocabulary(vocab);

	return bowDE;
}

void bowDE_features_description(BOWImgDescriptorExtractor bowDE, Mat* X, Mat* Y, string dir, int n_features = 128)
{
	int class_num = 0;
	int counter = 0;
	for (directory_entry& dec_class : directory_iterator(dir))
	{
		cout << dec_class.path() << " directory was opened.\n";
		for (directory_entry& img_path : directory_iterator(dec_class.path().string()))
		{
			//reading the image
			Mat img = imread(img_path.path().string());

			//convertion to grayscale
			Mat gray;
			cvtColor(img, gray, COLOR_BGR2GRAY);

			//image resizing
			resize(gray, gray, Size(500, 500), INTER_LINEAR);

			//SIFT local feature detection and histogram computation with bowDE
			vector<KeyPoint> keypoints;
			Ptr<SIFT> feature_extractor = SIFT::create(n_features);
			feature_extractor->detect(gray, keypoints);
			Mat descriptor;
			bowDE.compute(gray, keypoints, descriptor);
			X->push_back(descriptor);

			//labeling the image
			Y->push_back(class_num);

			if (counter % 100 == 0)
			{
				cout << counter << " images from the directory processed.\n";
			}
			counter++;
		}
		class_num++;
	}
}

void bowDE_features_classifier(BOWImgDescriptorExtractor bowDE, string dir_train,string dir_test, int n_features = 128)
{
	Mat trainX, trainY;

	//describing training images in BOW dictionary feature space (histograms)
	bowDE_features_description(bowDE, &trainX, &trainY, dir_train, n_features);

	//Initializing and training classifier
	cout << "Initializing and training classifier...\n";
	Ptr<SVM> svm = SVM::create();
	svm->setType(SVM::C_SVC);
	svm->setKernel(SVM::INTER);
	svm->setTermCriteria(TermCriteria(TermCriteria::MAX_ITER, 300, 1e-6));
	svm->train(trainX, ROW_SAMPLE, trainY);
	svm->save("trained_models/svm.xml");

	Mat testX,testY;
	bowDE_features_description(bowDE, &testX, &testY, dir_test, n_features);

	//testing classifier
	cout << "testing classifier...\n";
	Mat predRes;
	svm->predict(testX, predRes);
	predRes.convertTo(predRes, CV_32S);
	float accuracy = float(countNonZero(predRes == testY)) / float(testY.rows);

	//printing a bunch of test results
	for (int i = 0; i < testY.rows; i++)
	{
		cout << "true label: " << testY.at<float>(i, 0) << " prediction: " << predRes.at<float>(i, 0) << "\n";
	}
	cout << "\nclassification accuracy on "<<testY.rows<< " test images: " << accuracy;
}


void opencv_spooky_local_features_descriptor_extractor_full()
{
	//SIFT parameters initialization
	ofstream outFile;
	int n_features = 128;
	int vocab_size = 64;

	//Paths to train and test sets.
	string dir_train = "webinar_pics/webinar_pics_tt_split/train";
	string dir_test = "webinar_pics/webinar_pics_tt_split/test";

	//initializing Bag of words descriptor extractor and it's vocabulary on training set of images
	BOWImgDescriptorExtractor bowDE = bowDE_preparation(dir_train, n_features, vocab_size);

	//describing images in BOW dictionary feature space, training and testing SVM
	bowDE_features_classifier(bowDE, dir_train, dir_test, n_features);

	cout << "\nDone!\n";
	waitKey(0);
}

void bowDE_svm_prediction()
{
	//reading an image
	string img_path = "webinar_pics/webinar_pics_tt_split/test/skeletons/43.png";
	Mat img = imread(img_path);

	//convertion to grayscale
	Mat gray;
	cvtColor(img, gray, COLOR_BGR2GRAY);

	//image resizing
	resize(gray, gray, Size(500, 500), INTER_LINEAR);

	//SIFT local feature description 
	int n_features = 128;
	vector<KeyPoint> keypoints;
	Ptr<SIFT> feature_extractor = SIFT::create(n_features);
	feature_extractor->detect(gray, keypoints);
	
	//reading the vocabulary
	Mat vocab_read;
	FileStorage storage_r("trained_models/BOW_vocab.xml", FileStorage::READ);
	storage_r["vocabulary"] >> vocab_read;
	storage_r.release();

	//initializing bag of words descriptor extractor
	BOWImgDescriptorExtractor bowDE(SIFT::create(n_features), DescriptorMatcher::create("FlannBased"));
	
	//setting bowDE vocabulary
	bowDE.setVocabulary(vocab_read);

	//histogram computation
	Mat descriptor;
	bowDE.compute(gray, keypoints, descriptor);
	Mat X;
	X.push_back(descriptor);

	//loading the SVM
	Ptr<SVM> svm = SVM::create();
	svm = SVM::load("trained_models/svm.xml");

	//prediction
	Mat predRes;
	svm->predict(X, predRes);
	predRes.convertTo(predRes, CV_32S);
	switch (predRes.at<int>(0,0)) {
	case 0:
		cout << "\nghost\n";
		break;
	case 1:
		cout << "\npumpkin\n";
		break;
	case 2:
		cout << "\nskeleton\n";
		break;
	case 3:
		cout << "\nwitch\n";
		break;
	}
}