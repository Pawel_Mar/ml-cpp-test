# Overview
Project has simple design. Autonomous procedures stored in *_test.cpp files are run from main.cpp file. 

# File contents
## mlpack_test.cpp
Mlpack models and functionalities example procedures. File contains following functions:
- mlpack_rf_test - random forest test on german.csv trial data
- mlpack_svm_test - SVM test on german.csv trial data
- mlpack_k_means_test - k-means test on trial german.csv data
- mlpack_linear_regression_test - linear regression test on haloween spendings dataset https://figshare.com/articles/dataset/USA_Halloween_spending/13125572
- mlpack_rf_spooky_test - random forest test on Michal's dataset with features described by HOG global descriptor
- mlpack_k_means_spooky_test - k-means test on Michal's dataset with features described by HOG global descriptor (needs fixing)
- mlpack_svm_candy_test - svm and rf classifiers test on candy-data dataset (https://data.world/fivethirtyeight/candy-power-ranking/workspace/file?filename=candy-data.csv) with discretization of constant explained variable

## opencv_test.cpp
- opencv_images_descriptor - converting images to data matrix with HOG global descriptor (pneumonia dataset) and saving it to .csv
- opencv_classifier - opencv svm classifier on pneumonia dataset with HOG features
- opencv_spooky_images_descriptor - converting images to data matrix with HOG global descriptor (Michal's dataset)
- opencv_spooky_classifier - opencv svm classifier on Michal's dataset with HOG features
- opencv_spooky_local_features_descriptor_extractor_test - local features extraction (SIFT algorithm) test on pumpkins images from michal's dataset with detected features displaying
- opencv_spooky_local_features_descriptor_extractor_full - full iamge classification algorithm on michal's dataset (local features extraction and description with SIFT, bag of visual words, SVM classiffier)

## candy-data.csv
source: https://data.world/fivethirtyeight/candy-power-ranking/workspace/file?filename=candy-data.csv

candy-data.csv includes attributes for each candy along with its ranking. For binary variables, 1 means yes, 0 means no.

The data contains the following fields:
- Header:	Description
- chocolate:	Does it contain chocolate?
- fruity:	Is it fruit flavored?
- caramel:	Is there caramel in the candy?
- peanutalmondy:	Does it contain peanuts, peanut butter or almonds?
- nougat:	Does it contain nougat?
- crispedricewafer:	Does it contain crisped rice, wafers, or a cookie component?
- hard:	Is it a hard candy?
- bar:	Is it a candy bar?
- pluribus:	Is it one of many candies in a bag or box?
- sugarpercent:	The percentile of sugar it falls under within the data set.
- pricepercent:	The unit price percentile compared to the rest of the set.
- winpercent:	The overall win percentage according to 269,000 matchups.

## features_test.csv
File containing numerical features of test part of pneumonia dataset (https://www.kaggle.com/datasets/paultimothymooney/chest-xray-pneumonia , labels are saved in first column).

Features were extracted by HOG global descriptor OpenCV implementation (for more information see: https://learnopencv.com/histogram-of-oriented-gradients/ , https://learnopencv.com/handwritten-digits-classification-an-opencv-c-python-tutorial/)

## features_train.csv
File containing numerical features of train part of pneumonia dataset (https://www.kaggle.com/datasets/paultimothymooney/chest-xray-pneumonia , labels are saved in first column).

Features were extracted by HOG global descriptor OpenCV implementation (for more information see: https://learnopencv.com/histogram-of-oriented-gradients/ , https://learnopencv.com/handwritten-digits-classification-an-opencv-c-python-tutorial/)

## german.csv
Trial dataset of mlpack library. I don't know what's in it, only that the last column are labels :D.

## spending_totals.csv
source: https://figshare.com/articles/dataset/USA_Halloween_spending/13125572

Dataset containg US Haloween spendings on costumes, candy, decorations and cards (in billions?) noted every year since 2005 to 2020.

## spooky_features.csv
File containing numerical features of images in Michal's dataset (labels are saved in first column).

Features were extracted by HOG global descriptor OpenCV implementation (for more information see: https://learnopencv.com/histogram-of-oriented-gradients/ , https://learnopencv.com/handwritten-digits-classification-an-opencv-c-python-tutorial/)

## mymodel.xml
Sample mlpack model saved by one of mlpack procedures.
