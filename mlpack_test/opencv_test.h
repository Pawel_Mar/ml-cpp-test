#pragma once

void opencv_images_descriptor();
void opencv_classifier();
void opencv_spooky_classifier();
void opencv_spooky_images_descriptor();
void opencv_spooky_local_features_descriptor_extractor_test();
void opencv_spooky_local_features_descriptor_extractor_full();
void bowDE_svm_prediction();