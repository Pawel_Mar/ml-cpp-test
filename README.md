# ML cpp test

## Description
C++ machine learning training ground. Effects achieved are going to be used in webinar preparation.

## Visuals


## Installation
Project was developed and tested in VS 2019 on Windows 10. Should work fine also on later versions.

Vcpkg is recommended for installation. To install vcpkg follow: 
- https://www.youtube.com/watch?v=b7SdgK7Y510&ab_channel=TroubleChute

commands:
```
git clone https://github.com/microsoft/vcpkg
.\vcpkg\bootstrap-vcpkg.bat
```

Following packages are required:
- mlpack
- openCV
- boost-filesystem

Run following in vcpkg directory:
- ```.\vcpkg install mlpack --triplet=x64-windows``` https://www.mlpack.org/doc/mlpack-3.4.2/doxygen/build_windows.html
- ```.\vcpkg install opencv --triplet=x64-windows``` https://github.com/Nivratti/vcpkg_opencv
- ```.\vcpkg install boost-filesystem --triplet=x64-windows```

Using command ```.\vcpkg integrate project``` is recommended. Then, dependencies should be installed in vs project as it is shown in https://www.youtube.com/watch?v=b7SdgK7Y510&ab_channel=TroubleChute .

For test purposes some datasets too big for gitlab were used:
- https://www.kaggle.com/datasets/paultimothymooney/chest-xray-pneumonia
- https://siipoland-my.sharepoint.com/:f:/g/personal/mzaleski_sii_pl/Eu3raGMGN-FLvKkWiXh0O-QBjFI2PDo-Ac7DgDykFFj96Q?e=luSwDP

Make sure to unzip them in mlpack_test project subdirectory.
## Usage
Current project structure is simple. main.cpp is used to run autonomus scripts contained in modules (e.g. mlpack_test.cpp).

## License


## Project status
